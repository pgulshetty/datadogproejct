
# DataDogProject
Demo project to play with datadog events and metrics

This project aims to demonstrate how to utilize Datadog for collecting metrics and events, and leverage them to send alerts to a Slack channel when specific conditions are met. By following this example, you will gain an understanding of the workflow involved in setting up monitoring and alerting using Datadog.

When we send http request to create a user, we send a event to datadog and also have metric which increments numbero f users created data.

The key steps involved in this project are as follows:
* Collecting Metrics or Events: You will learn how to configure Datadog to collect custom metrics or events. These could be metrics related to system performance, application-specific metrics, or events triggered by specific occurrences within your application or infrastructure.

* Defining Alert Conditions: Once the metrics or events are collected, you will define the conditions that should trigger an alert. For example, you might set a threshold for CPU usage or specify a condition where the number of errors exceeds a certain value within a defined time window.

* Configuring Slack Integration: In order to receive alerts, you will integrate Datadog with Slack, a popular collaboration platform. This integration allows you to send alert notifications to a Slack channel of your choice, ensuring that the right stakeholders are promptly notified.

* Creating Alert Policies: You will define alert policies within Datadog, which specify the conditions and notification channels for each alert. These policies ensure that alerts are properly managed and delivered to the appropriate channels when triggered.

# Configurations required to push the datadog metric and APM details
1. Add your datadog API_KEY to application.properties file: `management.metrics.export.datadog.api-key=`
2. Need to use datadog APM jar file `dd-java-agent.jar` while running the application.
```bash
wget -O dd-java-agent.jar 'https://dtdg.co/latest-java-tracer'`
```

# How to run the application

## Build the application
```bash
mvn clean install
```
Build will produce a runnable JAR: `DataDogProject-1.0-SNAPSHOT.jar`

## Run application
```bash
 java -javaagent:{{path_to_dd-java-agent.jar}} -Ddd.logs.injection=true -jar target/DataDogProject-1.0-SNAPSHOT.jar

```

This project consists of 2 http end-points.
1. Register a user: send http request, request details are
```bash
 METHOD: POST
 URL: http://localhost:8080/users
 HEADERS: Content-type: application/json
 BODY:  {
            "name": "third-user",
            "email":"pgulshetty@gmail.com",
            "address":"amsterdam, Netherland"
        }
```
2. Produces a error
```bash
  METHOD: GET
  URL: http://localhost:8080/error
```
