package datadog;

import com.timgroup.statsd.NonBlockingStatsDClientBuilder;
import com.timgroup.statsd.StatsDClient;
import org.springframework.boot.actuate.autoconfigure.metrics.export.ConditionalOnEnabledMetricsExport;
import org.springframework.boot.actuate.autoconfigure.metrics.export.statsd.StatsdProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
public class EventConfig {
    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnEnabledMetricsExport("statsd")
    StatsDClient statsDClient(StatsdProperties statsdConfig) {
        String[] stingTags = {"service:DatadogService"};
        return new NonBlockingStatsDClientBuilder()
                .constantTags(stingTags)
                .hostname(statsdConfig.getHost())
                .port(statsdConfig.getPort())
                .maxPacketSizeBytes(statsdConfig.getMaxPacketLength())
                .build();
    }
}
