package datadog;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

@Service
public class DataDogMetricService {
    public static final String USER_COUNTER_NAME = "user.creation.counter";
    private final Counter userCreationCounter;

    public DataDogMetricService(MeterRegistry meterRegistry) {

        this.userCreationCounter = Counter.builder(USER_COUNTER_NAME)
                .tag("service", "datadog-metric-service")
                .description("Datadog metric service")
                .register(meterRegistry);
    }

    public void incrementCounter() {
        userCreationCounter.increment();
    }
}
