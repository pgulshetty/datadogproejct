package datadog;

import com.timgroup.statsd.Event;
import com.timgroup.statsd.StatsDClient;
import org.springframework.stereotype.Service;

import java.time.Clock;

@Service
public class DataDogEventService {

    private static final String TITLE = "user.create.event";
    private static final String TEXT = "User created";
    private static final String SIGNUP_ID_TAG_FORMAT = "userId:%s";
    private final StatsDClient statsDClient;
    private final Clock clock;

    public DataDogEventService(StatsDClient statsDClient, Clock clock) {
        this.statsDClient = statsDClient;
        this.clock = clock;
    }

    public void sendEventForCreatingUser(String userId) {
        Event duplicateAccount = Event.builder()
                .withTitle(TITLE)
                .withText(TEXT)
                .withDate(clock.millis())
                .withAlertType(Event.AlertType.WARNING)
                .build();

        String[] tags = buildTags(userId);

        statsDClient.recordEvent(duplicateAccount, tags);
    }

    private String[] buildTags(String userId) {
        return new String[]{
                String.format(SIGNUP_ID_TAG_FORMAT, userId),
        };
    }
}
