package service;

import Domain.User;
import datadog.DataDogEventService;
import datadog.DataDogMetricService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private final DataDogEventService dataDogEventService;
    private final DataDogMetricService dataDogMetricService;
    private final List<User> userList = new ArrayList<>();

    public UserService(DataDogEventService dataDogEventService, DataDogMetricService dataDogMetricService) {
        this.dataDogEventService = dataDogEventService;
        this.dataDogMetricService = dataDogMetricService;
    }

    public void createUser(User user) {
        userList.add(user);
        // send event of user created with userId as tag
        dataDogEventService.sendEventForCreatingUser(user.getUserId());
        // increment user metric count
        dataDogMetricService.incrementCounter();
    }
}
