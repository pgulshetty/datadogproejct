package controller;

import io.micrometer.core.instrument.Measurement;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static datadog.DataDogMetricService.USER_COUNTER_NAME;

@RestController
@RequestMapping("/metrics")
public class MetricsController {
    private final MeterRegistry meterRegistry;

    public MetricsController(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @GetMapping
    public List<Measurement> getMetrics() {
        return meterRegistry.getMeters()
                .stream()
                .filter(meter -> meter.getId().getName().equals(USER_COUNTER_NAME))
                .flatMap(meter -> StreamSupport.stream(meter.measure().spliterator(), false))
                .collect(Collectors.toList());
    }
}
