package Domain;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
    private String userId;
    private String email;
    private String userAddress;

    @JsonCreator
    public User(@JsonProperty("name") String userId,
                @JsonProperty("email") String email,
                @JsonProperty("address") String userAddress) {
        this.userId = userId;
        this.email = email;
        this.userAddress = userAddress;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }
}
